# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a small project to show my skills in Django.
The goal of this app is to register users, create tasks
and assign it to a existing user.
The users can mark theirs tasks as completed.
To do all those things the user must be loged in.

### Version ###

This is version: 0.1

### Dependencies ###

It's important to have an installed version of the python interpreter in the computer and access to a database, preferably MySQL, but SQLite will do the job too for testing purposes.

This app was created with Python 2.7 and Django 1.10, uses Django-Rest-Framework 3.5 to create API REST.

### Database configuration ###

* If you plan to use a MySQL database you must configure it the settings.py module located inside 'todo' directory, there you must write the conection parameters to a MySQL database.

* You can also use SQLite using the configuration commented in the settings.py module.

### Summary ###

* Clone the repository located in the url: https://bitbucket.org/fernando_gleon/todo-test.git

* Inside the folder created for this project, create a virtual environment to keep all the libraries neeed to run this application.

> virtualenv venv

* To activate virtualenv in windows: venv\scripts\activate

*  To activate virtualenv in linux: source venv/bin/activate

* Install python dependencies

> pip install -r requirements.txt

* Migrate tables in the DataBase

> python manage.py makemigrations

> python manage.py migrate

* Start development server

> python manage.py runserver

* In order to have data to work with at the beginning, you can import ToDo's records with the following instruction

> python manage.py loaddata todo.json


### Using the App ###

* Open your web browser and put in the address bar this url: http://localhost:8000/

* There are no users created at the beginning, so you must create at least one.

* Once a user is created you can login and start using the app, there are two menu items: A) ToDos and B) Users. They allow to make CRUD operation on Todos and Users.

* To assign a task to a user, just click to the 'Edit' button and select the user in the form.

* To mark a task as completed just click on the 'Complete' button.

* To logout just click to the far right icon in the menu bar.


### Do you need help? ###

You can email me to fguerrero11@gmail.com to discuss about this project or any other python and django related topic.