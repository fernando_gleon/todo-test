from django.forms import ModelForm
from .models import ToDo
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, Div

# Create the ModelForm class for ToDo.
class ToDoForm(ModelForm):

    # Injecting the 'form-control' class into each form's field
    # in order to bootstrap fields
    def __init__(self, *args, **kwargs):
        super(ToDoForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })    
    class Meta:
        model = ToDo
        fields = ['id', 'description', 'responsible']


# Create the ModelForm class for User.
class UserForm(ModelForm):

    # Injecting the 'form-control' class into each form's field
    # in order to bootstrap fields
    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
        })    
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Div(
                Div('first_name', 'last_name', 'email',
                    css_class='col-xs-6'),
                Div('username', 'password',
                    css_class='col-xs-6'),                 
            css_class='row-fluid'), 
        )        

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email', 'username', 'password']        