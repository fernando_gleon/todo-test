from django.conf.urls import url, include
from django.views.generic.base import TemplateView

from .views import ToDoList, ToDoDetail
from .views import UserList, UserDetail
from .views import ToDoView, UserView
from .views import CreateToDoView, EditToDoView, CompleteToDoView
from .views import CreateUserView, EditUserView, RegisterUserView
from .views import authenticate_user, logout_view, HomeView




urlpatterns = [

    url(r'^login/$', TemplateView.as_view(template_name='login.html'), name='login' ),
    url(r'^logout/$', logout_view, name='logout' ),
    url(r'^login_error/$', TemplateView.as_view(template_name='login_error.html'), name='login-error' ),
    url(r'^authenticate_user/$', authenticate_user, name='authenticate_user' ),


    url(r'^$', HomeView.as_view(), name='home' ),
    # url(r'^$', TemplateView.as_view(template_name='base.html'), name='base' ),
    url(r'^todos/$', ToDoView.as_view(), name='todos' ),
    url(r'^users/$', UserView.as_view(), name='users' ),
    url(r'^create/todo/$', CreateToDoView.as_view(), name='create-todo' ),
    url(r'^edit/todo/(?P<pk>[0-9]+)/$', EditToDoView.as_view(), name='edit-todo' ),

    url(r'^create/user/$', CreateUserView.as_view(), name='create-user' ),
    url(r'^edit/user/(?P<pk>[0-9]+)/$', EditUserView.as_view(), name='edit-user' ),
    url(r'^register/$', RegisterUserView.as_view(), name='register' ),

    url(r'^api/todos/$', ToDoList.as_view(), name='todo-list' ),
    url(r'^api/todos/(?P<pk>[0-9]+)/$', ToDoDetail.as_view(), name='todo-detail' ),
    url(r'^api/todos/(?P<pk>[0-9]+)/complete/$', CompleteToDoView.as_view(), name='complete-todo' ),

    url(r'^api/users/$', UserList.as_view(), name='user-list' ),
    url(r'^api/users/(?P<pk>[0-9]+)/$', UserDetail.as_view(), name='user-detail' ),    



]