from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect
from django.views.generic.base import TemplateView
# from django.shortcuts import render
from rest_framework import generics, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import ToDo
from .forms import ToDoForm, UserForm
from .serializers import ToDoSerializer, UserSerializer
# Create your views here.


def authenticate_user(request):
    print request.POST
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/')
    else:
        return redirect('/login_error/')


def logout_view(request):
    logout(request)
    return redirect('login')




class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "base.html"
    login_url = '/login/'



class ToDoView(LoginRequiredMixin, TemplateView):
    template_name = "todo.html"
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super(ToDoView, self).get_context_data(**kwargs)
        context['todos'] = ToDo.objects.all()
        return context


class CreateToDoView(LoginRequiredMixin, TemplateView):
    template_name = "form_todo_create.html"
    login_url = '/login/'
    # redirect_field_name = 'redirect_to'       

    def get_context_data(self, **kwargs):
        context = super(CreateToDoView, self).get_context_data(**kwargs)
        context['form'] = ToDoForm()
        return context        


class EditToDoView(LoginRequiredMixin, TemplateView):
    template_name = "form_todo_edit.html"
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super(EditToDoView, self).get_context_data(**kwargs)
        pk = kwargs.get('pk')
        instance=get_object_or_404( ToDo, pk=pk )
        context['form'] = ToDoForm(instance=instance)
        context['pk'] = pk
        return context   



class UserView(LoginRequiredMixin, TemplateView):
    template_name = "user.html"
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super(UserView, self).get_context_data(**kwargs)
        context['users'] = User.objects.order_by('-id')
        return context        


class CreateUserView(LoginRequiredMixin, TemplateView):
    template_name = "form_user_create.html"
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super(CreateUserView, self).get_context_data(**kwargs)
        context['form'] = UserForm()
        return context        


class RegisterUserView(TemplateView):
    template_name = "form_user_register.html"
    # login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super(RegisterUserView, self).get_context_data(**kwargs)
        context['form'] = UserForm()
        return context     


class EditUserView(LoginRequiredMixin, TemplateView):
    template_name = "form_user_edit.html"
    login_url = '/login/'

    def get_context_data(self, **kwargs):
        context = super(EditUserView, self).get_context_data(**kwargs)
        pk = kwargs.get('pk')
        instance=get_object_or_404( User, pk=pk )
        instance.password = ''
        context['form'] = UserForm(instance=instance)
        context['pk'] = pk
        return context   






class ToDoList( generics.ListCreateAPIView):
    permission_classes = ( permissions.IsAuthenticated, )

    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer


class ToDoDetail( generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = ( permissions.IsAuthenticated, )

    queryset = ToDo.objects.all()
    serializer_class = ToDoSerializer    


class CompleteToDoView(APIView):
    # permission_classes = ( permissions.IsAuthenticated, )

    def put(self, request, pk, format=None):
        todo = get_object_or_404( ToDo, pk=pk )
        if todo.responsible == request.user:
            todo.is_completed = True
            todo.save()
            print todo

            result = dict(
                status='success'
            )
        else:
            result = dict(
                status='failed'
            )            
        return Response(result)



class UserList( generics.ListCreateAPIView):
    # permission_classes = ( permissions.IsAuthenticated, )

    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail( generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = ( permissions.IsAuthenticated, )

    queryset = User.objects.all()
    serializer_class = UserSerializer      

