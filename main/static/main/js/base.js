$(function() {




    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });



    $(document).on("click", ".complete", function(e) {
        e.preventDefault();
        console.log("Alert Callback");

        var button = $(this);
        var pk = button.attr('data-id');
        // var pk = $(this).attr('data-id');
        $.ajax({        
            url:        '/api/todos/' + pk + '/complete/',
            type:       'PUT',     
            data:        null,
            dataType:   'JSON',      
            error:  function( jqXHR, textStatus, errorThrown ) {
                if ( textStatus != 'abort') {
                    alert( textStatus );
                    alert( errorThrown );                                           
                }
            },                      
            success:  function( resp ) {
                if ( resp.status == 'success' ) {
                    button.parent().parent().find('.is_completed').html('True');
                    bootbox.alert("This Task has been marked has Completed<br>Congratulations!");
                } else {
                    bootbox.alert("Sorry but, this Task is not assigned to you!");                    
                }
                // console.log('Task Completed');
                // console.log( button.parent().parent().find('.is_completed').html() );
                // location.href='/todos/';
            }
        }); 

        // bootbox.alert("This Task has been marked has Completed<br>Congratulations!", function() {

        // });
    });    


    $(document).on("click", ".accept_create_todo", function(e) {
        e.preventDefault();
        var data = $('#form_todo').serialize();
        $.ajax({        
            url:        '/api/todos/',
            type:       'POST',     
            data:        data,
            dataType:   'JSON',      
            error:  function( jqXHR, textStatus, errorThrown ) {
                if ( textStatus != 'abort') {
                    // alert( textStatus );
                    // alert( errorThrown );  
                    bootbox.alert("There is an error, please provide all the information required in this form!");                                          
                }
            },                      
            success:  function( resp ) {
                console.log('Success');
                location.href='/todos/';
            }
        }); 
    });  

    $(document).on("click", ".accept_edit_todo", function(e) {
        e.preventDefault();
        // var data = $('#form_todo').serialize();
        var data = new FormData( $('#form_todo')[0] );
        var pk = $(this).attr('data-id');
        console.log( pk );
        $.ajax({        
            url:        '/api/todos/' + pk + '/',
            type:       'PUT',     
            data:        data,

            cache:  false,
            contentType: false,
            processData: false,

            dataType:   'JSON',      
            error:  function( jqXHR, textStatus, errorThrown ) {
                if ( textStatus != 'abort') {
                    alert( textStatus );
                    alert( errorThrown );                                           
                }
            },                      
            success:  function( resp ) {
                console.log('Success');
                location.href='/todos/';
            }
        }); 
    });          

    $(document).on("click", ".delete_todo", function(e) {
        e.preventDefault();
        var pk = $(this).attr('data-id');
        console.log( pk );
        $.ajax({        
            url:        '/api/todos/' + pk + '/',
            type:       'DELETE',     
            dataType:   'JSON',      
            error:  function( jqXHR, textStatus, errorThrown ) {
                if ( textStatus != 'abort') {
                    alert( textStatus );
                    alert( errorThrown );                                           
                }
            },                      
            success:  function( resp ) {
                console.log('Success');
                location.href='/todos/';
            }
        }); 
    });    



    $(document).on("click", ".accept_create_user", function(e) {
        e.preventDefault();
        var data = $('#form_user').serialize();
        $.ajax({        
            url:        '/api/users/',
            type:       'POST',     
            data:        data,
            dataType:   'JSON',      
            error:  function( jqXHR, textStatus, errorThrown ) {
                if ( textStatus != 'abort') {
                    // alert( textStatus );
                    // alert( errorThrown );   
                    bootbox.alert("There is an error, please provide all the information required in this form!"); 
                }
            },                      
            success:  function( resp ) {
                console.log('Success');
                location.href='/users/';
            }
        }); 
    });  

    $(document).on("click", ".accept_edit_user", function(e) {
        e.preventDefault();
        // var data = $('#form_user').serialize();
        var data = new FormData( $('#form_user')[0] );
        var pk = $(this).attr('data-id');
        console.log( pk );
        $.ajax({        
            url:        '/api/users/' + pk + '/',
            type:       'PUT',     
            data:        data,

            cache:  false,
            contentType: false,
            processData: false,

            dataType:   'JSON',      
            error:  function( jqXHR, textStatus, errorThrown ) {
                if ( textStatus != 'abort') {
                    alert( textStatus );
                    alert( errorThrown );                                           
                }
            },                      
            success:  function( resp ) {
                console.log('Success');
                location.href='/users/';
            }
        }); 
    });          
    
    $(document).on("click", ".delete_user", function(e) {
        e.preventDefault();
        var pk = $(this).attr('data-id');
        console.log( pk );
        $.ajax({        
            url:        '/api/users/' + pk + '/',
            type:       'DELETE',     
            dataType:   'JSON',      
            error:  function( jqXHR, textStatus, errorThrown ) {
                if ( textStatus != 'abort') {
                    alert( textStatus );
                    alert( errorThrown );                                           
                }
            },                      
            success:  function( resp ) {
                console.log('Success');
                location.href='/users/';
            }
        }); 
    });       


    
});