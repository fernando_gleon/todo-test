from django.contrib.auth.models import User
from rest_framework import serializers
from .models import ToDo

class ToDoSerializer( serializers.ModelSerializer ):
    class Meta:
        model = ToDo
        fields = ( 'id', 'description', 'responsible')  
        # fields = ( 'id', 'description', 'responsible', 'created')  


class UserSerializer( serializers.ModelSerializer ):
    class Meta:
        model = User
        fields = ( 'id', 'first_name', 'last_name', 'email', 'username', 'password', 'is_superuser')          
        extra_kwargs = {'password': {'write_only': True, 'allow_null':True}  }

    def create(self, validated_data):    
        user = User.objects.create( **validated_data ) 
        user.set_password(validated_data['password'])
        user.save()   
        return user       

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name  = validated_data.get('last_name', instance.last_name)
        instance.email      = validated_data.get('email', instance.email)
        instance.username   = validated_data.get('username', instance.username)
        pswd = validated_data.get('password')
        if pswd is not None:
            instance.set_password( pswd )        
        instance.save()        
        return instance