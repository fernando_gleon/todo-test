from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.conf import settings

# Create your models here.


class ToDo(models.Model):
    description  = models.CharField(max_length=256)
    responsible  = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, blank=True, null=True)
    is_completed = models.BooleanField( default=False )
    
    created      = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return '{}'.format(self.description)  